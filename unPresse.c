#include <stdlib.h>
#include <stdio.h>

int unPresse(char file_name[]){
    FILE *fichier = NULL;
    FILE *funPresse = NULL;
    int nbCaracPareil;
    int carac;
    fichier = fopen(file_name, "r");
    if(fichier != NULL){
        funPresse = fopen("resultat.txt", "w");
        int nbCaracPareil = fgetc(fichier);
        int carac = fgetc(fichier);
        do{
            int i;
            for(i = 1; i <= nbCaracPareil; ++i){
                fputc(carac, funPresse);
            }
            nbCaracPareil = fgetc(fichier);
            carac = fgetc(fichier);
        }while(nbCaracPareil != EOF && carac != EOF);
        fclose(funPresse);
        fclose(fichier);
    } else {
        printf("Erreur! Le fichier à décompresser n'existe pas!'");
    }
}
