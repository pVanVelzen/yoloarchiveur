#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "comprime.h"
#include "unPresse.h"

int main(int argc, char *argv[]){
    //unPresse file
    if(argc == 3){
        if(strcmp(argv[1],"-x") == 0){
            unPresse(argv[2]);
        } else {
            printf("Mauvais argument entré, veuillez vous référer au README\n.");
            return 1;    
        }
    //comprime 
    }else if(argc == 5){
        //comprime un fichier
        if(strcmp(argv[1],"-a") == 0){
            if(strcmp(argv[3],"-o") == 0){
                comprime(argv[2], argv[4]);
            } else {
                printf("%s\n",argv[4]);
                return 1;
            }
        //comprime un dosier
        } else if (strcmp(argv[1], "-ar") == 0){
            if(strcmp(argv[3],"-o") == 0){
                comprime(argv[2], argv[4]);
            }
        } else {
            printf("Mauvais argument entré, veuillez vous référer au README\n.");
            return 1;
        }
    //Erreur mauvais nommbres d'arguments
    } else {
        printf("Vous avez entré unn mauvais nombre d'argument, veuillez vous référer au README.\n");
        return 1;
    }
    return 0;
}
