yoloArchiveur.out:yoloArchiveur.o comprime.o unPresse.o
	gcc -o yoloArchiveur.out yoloArchiveur.o comprime.o unPresse.o

yoloArchiveur.o:yoloArchiveur.c
	gcc -c yoloArchiveur.c -o yoloArchiveur.o

comprime.o:comprime.c
	gcc -c comprime.c -o comprime.o

unPresse.o:unPresse.c
	gcc -c unPresse.c -o unPresse.o

clean:
	rm -rf *.o *.out
