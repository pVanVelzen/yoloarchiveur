/*Fonction de compression des fichiers
 * char start_name[]: nom du fichier à archiver
 * char final_name[]: nom du fichier archivé
 *
 * La fonction va créer un fichier qui contiendra les information archivé.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int comprime(char start_name[], char final_name[]){
    FILE *fichier = NULL; 
    FILE *fCompresse = NULL;
    fichier = fopen(start_name,"r");
    if(fichier != NULL){
         int nbCaracPareil = 1;
         int carac = fgetc(fichier);
         int carac2 = fgetc(fichier);
         fCompresse = fopen(final_name, "w");
         while(carac != EOF && carac2 != EOF){
             if(carac != carac2){
                  fputc(nbCaracPareil, fCompresse);
                  fputc(carac, fCompresse);
                  carac = carac2;
                  nbCaracPareil = 1;
             } else {
                  nbCaracPareil = nbCaracPareil + 1;
             }
             carac2 = fgetc(fichier);
         } 
    fclose(fichier);
    fclose(fCompresse);
    } else {
        printf("Erreur à l'ouverture du fichier.\n"); 
        return 1;
    }
    return 0;
}
