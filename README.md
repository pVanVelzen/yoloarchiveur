#yoloarchiveur est simple archiveur de fichier.
Il utilise l'algorithme RLE pour la compression des fichiers. Éventuellement, yoloArchiveu devrait aussi être capable de compresser des dossiers,
mais la fonctionnalité n'est pas encore implenté.

##Utilisation

Pour l'instant, pour pouvoir compresser et décompresser des fichiers, il faut qu'il se trouve dans le même dossier que l'éxecutable.
Les fichiers compressé et décompressé seront aussi généré dans ce dossier.

##Installation
1. cloner le repository git sur votre ordinateur.
2. ce rendre dans le dossier et exécuter la commande make.

##Commande
-a: Archivé un fichier.
-ar: Archivé un dossier.(Non fonctionnelle)
-o: Nommé le fichier archivé.
-x: dé-archiver un fichier.
./yoloArchiveur: Exécute le programe.

*exemple:*
./yoloArchiveur -a file.txt -o file.arch
./yoloArchiveur -x file.arch
